import { Component, h } from 'preact';

import { BarCode, BarCodes, BaseOptions, IBarCodeProps, observer } from './barCode';

@observer
export class MSI extends Component<IBarCodeProps<BaseOptions> & { variant?: '10' | '11' | '1010' | '1110' }, any> {
  public render() {
    return <BarCode {...this.props}  type={this.type} />;
  }

  private get type(): BarCodes {
    if (this.props.variant) {
      return `MSI${this.props.variant}` as BarCodes;
    }

    return 'MSI';
  }
}
