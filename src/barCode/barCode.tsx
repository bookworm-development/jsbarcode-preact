import { action, computed } from 'mobx';
export { observer } from 'mobx-preact';
import { Component, h } from 'preact';

import * as JsBarcode from 'jsbarcode';

export interface IBarCodeProps<Options extends JsBarcode.BaseOptions> {
  containerType?: 'canvas' | 'image' | 'svg';
  value: string;
  options?: Options;
}

export type BaseOptions = JsBarcode.BaseOptions;

export type BarCodes =
  'CODE128' | 'CODE128A' | 'CODE128B' | 'CODE128C' |
  'EAN13' | 'EAN8' | 'EAN5' | 'EAN2' | 'UPC' |
  'CODE39' |
  'ITF' | 'ITF14' |
  'MSI' | 'MSI10' | 'MSI11' | 'MSI1010' | 'MSI1110' |
  'pharmacode' |
  'codabar';

export type Code128Options = JsBarcode.Code128Options;
export type Ean13Options = JsBarcode.Ean13Options;
export type Ean8Options = JsBarcode.Ean8Options;

export abstract class BarCode<P> extends Component<IBarCodeProps<P> & { type: BarCodes; }, any> {
  public componentDidMount() {
    // Grab a reference to the element
    const element = this.base as HTMLElement;

    this.generateBarcode(element);
  }

  // Update the barcode on value updates
  public componentDidUpdate() {
    // Grab a reference to the element
    const element = this.base as HTMLElement;

    this.generateBarcode(element);
  }

  public render() {
    return this.container;
  }

  @computed
  private get container() {
    switch (this.props.containerType) {
      case 'canvas':
        return <canvas />;

      case 'image':
        return <img />;

      default:
        return <svg />;
    }
  }

  @action
  private generateBarcode(el: HTMLElement) {
    try {
      JsBarcode(el, this.props.value, {
        ...(this.props.options || {}),
        format: this.props.type,
      });
    } catch (e) {
      this.clear(el);
    }
  }

  private clear(e: HTMLElement) {
    e.innerHTML = '';
  }
}
