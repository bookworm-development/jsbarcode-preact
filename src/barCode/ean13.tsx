import { Component, h } from 'preact';

import { BarCode, Ean13Options, IBarCodeProps, observer } from './barCode';

@observer
export class Ean13 extends Component<IBarCodeProps<Ean13Options>, any> {
  public render() {
    return <BarCode {...this.props}  type='EAN13' />;
  }
}
