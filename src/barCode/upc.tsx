import { Component, h } from 'preact';

import { BarCode, BaseOptions, IBarCodeProps, observer } from './barCode';

@observer
export class UPC extends Component<IBarCodeProps<BaseOptions>, any> {
  public render() {
    return <BarCode {...this.props}  type='UPC' />;
  }
}
