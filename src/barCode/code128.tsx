import { Component, h } from 'preact';

import { BarCode, BarCodes, Code128Options, IBarCodeProps, observer } from './barCode';

@observer
export class Code128 extends Component<IBarCodeProps<Code128Options>  & { variant?: 'A' | 'B' | 'C' }, any> {
  public render() {
    return <BarCode {...this.props}  type={this.type} />;
  }

  private get type(): BarCodes  {
    if (this.props.variant) {
      return `CODE128${this.props.variant}` as BarCodes;
    }

    return 'CODE128';
  }
}
