import { Component, h } from 'preact';

import { BarCode, Ean8Options, IBarCodeProps, observer } from './barCode';

@observer
export class Ean8 extends Component<IBarCodeProps<Ean8Options>, any> {
  public render() {
    return <BarCode {...this.props}  type='EAN8' />;
  }
}
