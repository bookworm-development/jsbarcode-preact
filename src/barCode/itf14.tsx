import { Component, h } from 'preact';

import { BarCode, BaseOptions, IBarCodeProps, observer } from './barCode';

@observer
export class ITF14 extends Component<IBarCodeProps<BaseOptions>, any> {
  public render() {
    return <BarCode {...this.props}  type='ITF14' />;
  }
}
